.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

CREATE_NEW equ 1
GENERIC_READ equ 80000000h
GENERIC_WRITE equ 40000000h
OPEN_EXISTING equ 3
CREATE_ALWAYS equ 2

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
atoi PROTO :DWORD
wypelnijRejestr PROTO
ScanInt PROTO 
lstrlenA PROTO :DWORD
walka PROTO 
GetTickCount PROTO
nseed PROTO :DWORD
nrandom PROTO :DWORD
maszynaLosujaca PROTO
tworzeniePlik PROTO
CreateFileA PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadFile PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
WriteFile PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
CloseHandle PROTO :DWORD
czytanieWlasciwosci PROTO :DWORD
StdOut PROTO :DWORD
dodaniePunktu PROTO  :DWORD , :DWORD
wykonanieCiosu PROTO   :DWORD ,  :DWORD
metodaCiosuTragicznego PROTO  :DWORD
 metodacosSieUdalo PROTO  :DWORD
 metodaudaloSie PROTO  :DWORD
 metodaprawiePerfect PROTO  :DWORD
 metodaciosPERFECT PROTO  :DWORD
  uczieczkaMetoda PROTO :DWORD ,:DWORD
.DATA
		write DWORD ?
		read DWORD ?
		l1 BYTE "     _________________________________________________________",10,0
		l2 BYTE "   /|     -_-                                             _-  |\",10,0
		l3 BYTE "  / |_-_- _                                         -_- _-   -| \  ",10,0
		l4 BYTE "    |                            _-  _--                      | ",10,0
		l5 BYTE "    |                            ,                            |",10,0
		l6 BYTE "    |      .-'````````'.        '(`        .-'```````'-.      |",10,0
		l7 BYTE "    |    .` |           `.      `)'      .` |           `.    |",10,0
		l8 BYTE "    |   /   |   ()        \      U      /   |    ()       \   |",10,0
		l9 BYTE "    |  |    |    ;         | o   T   o |    |    ;         |  |",10,0
		l10 BYTE "    |  |    |     ;        |  .  |  .  |    |    ;         |  |",10,0
		l11 BYTE "    |  |    |     ;        |   . | .   |    |    ;         |  |",10,0
		l12 BYTE "    |  |    |     ;        |    .|.    |    |    ;         |  |",10,0
		l13 BYTE "    |  |    |____;_________|     |     |    |____;_________|  |",10,0
		l14 BYTE "    |  |   /  __ ;   -     |     !     |   /     `'() _ -  |  |",10,0
		l15 BYTE "    |  |  / __  ()        -|        -  |  /  __--      -   |  |",10,0
		l16 BYTE "    |  | /        __-- _   |   _- _ -  | /        __--_    |  |",10,0
		l17 BYTE "    |__|/__________________|___________|/__________________|__|",10,0
		l18 BYTE "   /                                             _ -        lc \",10,0
		l19 BYTE "  /   -_- _ -             _- _---                       -_-  -_ \",10,0


		wprowadzenie BYTE "... Nagle poczul uklucie, jednak wszystko wydawalo mu sie zamglone,",10,0 
		wprowadzenie2 BYTE ", byl nieobecny, uklucia jednak namnarzaly sie i przybieraly na sile, wraz z agresja to ona prostowala zwoje i przywracala zmysly wszystko trwalo okropnie dlugo gdy toczyly sie sekundy.",10,0
		wprowadzenie3 BYTE "Zmysl jedank gleboko zakorzeniony wyprowadzil go z amoku    ",10,0
		spacja BYTE 10,0
		bez BYTE "Bohater bez imienia ",10,0
		wprowadzenier DWORD $ - wprowadzenie
		wprowadzenier2 DWORD $ - wprowadzenie2
		wprowadzenier3 DWORD $ - wprowadzenie3
		lZnakow DWORD 0
		bufor BYTE 10 DUP(0)

		znaki BYTE "======================================================",10,0
		napis BYTE "                      WALKA",10,0
		miedzy BYTE "                   Miedzy ",10,0
		a BYTE "                     A,",10,0

		mozliwosci BYTE "Mozliwsoci: ",10,0
		mysl BYTE "1. Mysl",10,0
		walcz BYTE "2. Walcz",10,0
		szukaj BYTE "3. Szukaj",10,0
		uciekaj BYTE "4. Uciekaj",10,0

		myslenie BYTE "Postanowil myslec: ",10,0
		walczeie BYTE "Postanowil walczyc: ",10,0
		szukanie BYTE "Postanowil szukac: ",10,0
		przeciwnik1 BYTE "Szczur",10,0

		level DWORD 0
		imie BYTE 10 DUP(0)

		zakres DWORD 100
		wylosowana DWORD 0

		lokalizacja BYTE "c:\Postac.dat",0
		writeH DWORD ?

		buforo BYTE 4 DUP(0)
		licznik DWORD  0

		ciosOdPostac BYTE "Twoja postac wykonala cios o wartosci: %i",10,0
		ciosOdPrzeciwnika BYTE "Przecinwnik wykonala cios o wartosci: %i",10,0

		danePostaciUsunuete BYTE "Dane postaci usunuete",10,0

		wartoscR BYTE "Potencjal tej umiejetnosci: %i",10,0
		wartoscRPO BYTE "Wartosc umiejetnosci wynosi teraz: %i",10,0
		bufor2 BYTE 255 DUP(?)

		zyciePrzecwnika DWORD 100
		zyciePostaci DWORD 100

		przeciwnikUmarl BYTE "Przeciwnik umarl",10,0
		postacUmarl BYTE "Postac umarl",10,0

		blokada DWORD 0
		ucieklTekst BYTE "Postac uciekla, uniknela ciosu",10,0
		nieucieklTekst BYTE "Postac nie uciekla",10,0

		ciosPERFECT BYTE "Mysl byla tak potezna ze postac odkryla inny wymiar i wykonala cios!",10,0
		ciosPERFECTW BYTE "Cios byl tak potezny ze przeciwnik przez moment stracil swiadomosc",10,0
		ciosPERFECTS BYTE "Bohater znalazl zwloki innego wroga i uderzyl preciwnika poteznie!",10,0
		ciosTRAGICZNY BYTE "Bohater nic nie wymyslil!",10,0
		ciosTRAGICZNYW BYTE "Bohater nie trafil piescia :( ",10,0
		ciosTRAGICZNYS BYTE "Bohate nic nie znalazl ",10,0
		cioscosSieUdalo BYTE "Bohater pomyslal zeby walczyc!",10,0
		cioscosSieUdaloW BYTE "Bohater niechcacy trafil piescia we wroga!",10,0
		cioscosSieUdaloS BYTE "Bohater znalazl w sobie sile i zatakowal przeciwnika",10,0
		ciosudaloSie BYTE "Bohater odszukal slaby punkt przeciwnika i wykonal cios!",10,0
		ciosudaloSieW BYTE "Bohater trafil wroga!",10,0
		ciosudaloSieS BYTE "Bohater odszukal tepy przedmiot i zatakowal nim wroga!",10,0
		ciosprawiePerfect BYTE "Bohater uzyl kajdanow aby zatakowac wroga!",10,0
		ciosprawiePerfectW BYTE "Bohater uzyl idealnie wymierzyl i uderzyl wroga!",10,0
		ciosprawiePerfectS BYTE "Bohater znalaz noz ktorym zatakowal wroga!",10,0
.CODE

StdHandle MACRO wejscie :REQ, wyjscie :REQ

	invoke 	GetStdHandle, wejscie
	mov wyjscie, EAX
	
ENDM

WriteM MACRO w :REQ, tekst :REQ, rozmiar :REQ, liczba :REQ

	invoke WriteConsoleA, w,  tekst, rozmiar,  liczba,0

	
ENDM
ReadM MACRO r :REQ, bufor :REQ, liczba :REQ

	invoke ReadConsoleA, r, OFFSET bufor, 10, OFFSET lZnakow, 0

ENDM	

main proc
	
	

	StdHandle STD_INPUT_HANDLE, read

	StdHandle STD_OUTPUT_HANDLE, write

 invoke GetTickCount
 invoke nseed, EAX


		WriteM write, OFFSET l1, LENGTHOF l1, OFFSET  lZnakow
		WriteM write, OFFSET l2, LENGTHOF l2, OFFSET  lZnakow
		WriteM write, OFFSET l3, LENGTHOF l3, OFFSET  lZnakow
		WriteM write, OFFSET l4, LENGTHOF l4, OFFSET  lZnakow
		WriteM write, OFFSET l5, LENGTHOF l5, OFFSET  lZnakow
		WriteM write, OFFSET l6, LENGTHOF l6, OFFSET  lZnakow
		WriteM write, OFFSET l7, LENGTHOF l7, OFFSET  lZnakow
		WriteM write, OFFSET l8, LENGTHOF l8, OFFSET  lZnakow
		WriteM write, OFFSET l9, LENGTHOF l9, OFFSET  lZnakow
		WriteM write, OFFSET l10, LENGTHOF l10, OFFSET  lZnakow
		WriteM write, OFFSET l11, LENGTHOF l11, OFFSET  lZnakow
		WriteM write, OFFSET l12, LENGTHOF l12, OFFSET  lZnakow
		WriteM write, OFFSET l13, LENGTHOF l13, OFFSET  lZnakow
		WriteM write, OFFSET l14, LENGTHOF l14, OFFSET  lZnakow
		WriteM write, OFFSET l15, LENGTHOF l15, OFFSET  lZnakow
		WriteM write, OFFSET l16, LENGTHOF l16, OFFSET  lZnakow
		WriteM write, OFFSET l17, LENGTHOF l17, OFFSET  lZnakow
		WriteM write, OFFSET l18, LENGTHOF l18, OFFSET  lZnakow
		WriteM write, OFFSET l19, LENGTHOF l19, OFFSET  lZnakow

		WriteM write, OFFSET spacja, LENGTHOF spacja, OFFSET  lZnakow
		WriteM write, OFFSET spacja, LENGTHOF spacja, OFFSET  lZnakow
		WriteM write, OFFSET spacja, LENGTHOF spacja, OFFSET  lZnakow
		WriteM write, OFFSET wprowadzenie, LENGTHOF wprowadzenie, OFFSET  lZnakow
		WriteM write, OFFSET wprowadzenie2, LENGTHOF wprowadzenie2, OFFSET  lZnakow
		WriteM write, OFFSET wprowadzenie3, LENGTHOF wprowadzenie3, OFFSET  lZnakow
		mov EAX, OFFSET przeciwnik1
		invoke walka

	invoke CloseHandle, writeH

	invoke ExitProcess, 0


main endp


walka proc  
push EAX
   WriteM write, OFFSET znaki, LENGTHOF znaki, OFFSET  lZnakow
   WriteM write, OFFSET napis, LENGTHOF napis, OFFSET  lZnakow
   WriteM write, OFFSET miedzy, LENGTHOF miedzy, OFFSET  lZnakow
	mov AL, imie
	mov BL, 0
	cmp AL, BL
	je BezImienia
	
	
	powrot:
	WriteM write, OFFSET a, LENGTHOF a, OFFSET  lZnakow
	pop EAX

	WriteM write, eax, 10, OFFSET  lZnakow
; Tutaj musi byc fight loop
walkaTrwa:
 invoke CreateFileA, OFFSET lokalizacja, GENERIC_READ OR GENERIC_WRITE,0 , 0, OPEN_EXISTING,0 , 0
 mov writeH, EAX
	WriteM write, OFFSET znaki, LENGTHOF znaki, OFFSET  lZnakow
	WriteM write, OFFSET mozliwosci, LENGTHOF mozliwosci, OFFSET  lZnakow
	WriteM write, OFFSET mysl, LENGTHOF mysl, OFFSET  lZnakow
	WriteM write, OFFSET walcz, LENGTHOF walcz, OFFSET  lZnakow
	WriteM write, OFFSET szukaj, LENGTHOF szukaj, OFFSET  lZnakow
	WriteM write, OFFSET uciekaj, LENGTHOF uciekaj, OFFSET  lZnakow
	ReadM read, OFFSET bufor, OFFSET lZnakow
	mov EBX, OFFSET bufor
	add EBX,lZnakow
	mov [EBX-2], BYTE PTR 0
	invoke atoi, OFFSET bufor

		cmp al,1d
		je myslW
		cmp al,2d
		je walczW
		cmp al,3d
		je szukajW
		cmp al,4d
		je uciekajW
		cmp al,9d
		je clearUmiejetnosci

healtInsuranceCheck:
; tutaj cios przeciwnika
mov EBX, blokada
cmp EBX, 0d
jne brakCiosu
invoke wykonanieCiosu,1 , 10
poCIosie:
cmp zyciePostaci, 0d
jle PostacumarlA
cmp zyciePrzecwnika, 0d
jle PrzeciwnikumarlA
jmp czyszczenieSocketa
;Tutaj musi byc sprawdzenie czy ktos nie umarl jak nie to HERE WE ARE AGEIN
brakCiosu:
mov blokada, 0
jmp poCIosie
czyszczenieSocketa:
invoke CloseHandle, writeH
jmp walkaTrwa
	PrzeciwnikumarlA:
	invoke StdOut ,OFFSET przeciwnikUmarl
	jmp Powrot2
	PostacumarlA:
	invoke StdOut ,OFFSET postacUmarl
	jmp Powrot2

		Powrot2:
	ret

myslW:
WriteM write, OFFSET myslenie, LENGTHOF myslenie, OFFSET  lZnakow
invoke maszynaLosujaca
invoke czytanieWlasciwosci, 0
jmp healtInsuranceCheck
walczW:
invoke StdOut ,OFFSET walczeie
invoke maszynaLosujaca
invoke czytanieWlasciwosci, 1
jmp healtInsuranceCheck
szukajW:
invoke StdOut ,OFFSET szukanie
invoke maszynaLosujaca
invoke czytanieWlasciwosci, 2
jmp healtInsuranceCheck
uciekajW:
invoke StdOut ,OFFSET szukanie
invoke maszynaLosujaca
invoke czytanieWlasciwosci, 3
jmp healtInsuranceCheck

clearUmiejetnosci:
invoke StdOut ,OFFSET danePostaciUsunuete
invoke CloseHandle, writeH
invoke CreateFileA, OFFSET lokalizacja, GENERIC_READ OR GENERIC_WRITE,0 , 0, CREATE_ALWAYS,0 , 0
mov writeH, EAX
invoke CloseHandle, writeH
jmp walkaTrwa
 BezImienia:
 WriteM write, OFFSET bez, LENGTHOF bez, OFFSET  lZnakow
 jmp powrot

 walka endp

 maszynaLosujaca proc


 invoke nrandom, zakres
 mov wylosowana, EAX
; invoke czytanieWlasciwosci

 ret
 maszynaLosujaca endp

 czytanieWlasciwosci proc r :DWORD
 invoke ReadFile, writeH, OFFSET buforo, LENGTHOF buforo, OFFSET lZnakow, 0
 invoke CloseHandle, writeH
  invoke CreateFileA, OFFSET lokalizacja, GENERIC_READ OR GENERIC_WRITE,0 , 0, CREATE_ALWAYS,0 , 0
 mov writeH, EAX
 cmp lZnakow, 0
; je inicjacjaPliku
; pop EAX

 maszynaLosujacaP:
;invoke wsprintfA, OFFSET bufor2, OFFSET wartoscR, r
;invoke StdOut ,OFFSET bufor2

;mov ECX, lengthof buforo
;peloo:
	;push ECX
	;dec ECX
	mov ECX,R
	invoke wsprintfA, OFFSET bufor2, OFFSET wartoscR, [buforo+ECX]
	invoke StdOut , OFFSET bufor2

;	pop ECX
;loop peloo

mov EAX, wylosowana
mov EBX, r
cmp [buforo+EBX],AL
jl nieTaK
invoke metodaciosPERFECT, r
invoke dodaniePunktu ,0 , 10
invoke uczieczkaMetoda, r, 9
mov EBX,r
cmp EBX, 3d
je doRetaCW
invoke wykonanieCiosu,0 , 40
jmp doRetaCW
nieTaK:
cmp AL, 50d
jg zupelnieSlabo
cmp AL, 30d
jg cosSieUdalo
cmp AL,20d
jg udaloSie
cmp AL,0d
jg prawiePerfect
jmp doRetaCW
zupelnieSlabo:
invoke metodaCiosuTragicznego, r
invoke dodaniePunktu ,r , 0
invoke uczieczkaMetoda, r, 6
mov EBX,r
cmp EBX, 3d
je doRetaCW
invoke wykonanieCiosu,0 , 0
jmp doRetaCW
cosSieUdalo:
invoke metodacosSieUdalo, r
invoke dodaniePunktu ,r , 1
invoke uczieczkaMetoda, r, 6
mov EBX,r
cmp EBX, 3d
je doRetaCW
invoke wykonanieCiosu ,0 , 10
jmp doRetaCW
udaloSie:
invoke metodaudaloSie, r
invoke dodaniePunktu ,r , 2
invoke uczieczkaMetoda, r, 1
mov EBX,r
cmp EBX, 3d
je doRetaCW
invoke wykonanieCiosu ,0 , 20
jmp doRetaCW
prawiePerfect:
invoke metodaprawiePerfect, r
invoke dodaniePunktu ,r , 5
invoke uczieczkaMetoda, r, 2
mov EBX,r
cmp EBX, 3d
je doRetaCW
invoke wykonanieCiosu, 0 , 30
jmp doRetaCW
ucieczka:


doRetaCW:
 ret
 inicjacjaPliku:
	mov EDI, OFFSET buforo
	mov ECX, 4
	Wypelnij:
	push ECX
		mov AL,0
		stosb
		
	pop ECX
	loop Wypelnij
	invoke WriteFile, writeH, OFFSET buforo, LENGTHOF buforo, OFFSET lZnakow, 0
 jmp maszynaLosujacaP
 czytanieWlasciwosci endp

 dodaniePunktu PROC  ktory :DWORD , ile :DWORD
 mov EBX, ktory
 mov AL, [buforo+EBX]
 ADD EAX,ile
 mov [buforo+EBX], AL
; mov ECX, lengthof buforo
;peloo:
	;push ECX
;	dec ECX
	mov ECX,ktory
	invoke wsprintfA, OFFSET bufor2, OFFSET wartoscRPO, [buforo+ECX]
	invoke StdOut , OFFSET bufor2

	;pop ECX
;loop peloo

 invoke WriteFile, writeH, OFFSET buforo, LENGTHOF buforo, OFFSET lZnakow, 0
 ret
 dodaniePunktu endp

 wykonanieCiosu PROC  kto :DWORD , zaIle :DWORD
 mov EAX, kto
 cmp kto, 0d
 jne dostniePostac
 invoke wsprintfA, OFFSET bufor2, OFFSET ciosOdPostac, zaIle
 invoke StdOut ,OFFSET bufor2
 mov EAX,zyciePrzecwnika
 mov EBX, zaIle
 sub EAX,EBX
 mov zyciePrzecwnika, EAX
 jmp koniecWykonanieCiosu
 dostniePostac:
 invoke wsprintfA, OFFSET bufor2, OFFSET ciosOdPrzeciwnika, zaIle
 invoke StdOut ,OFFSET bufor2
 mov EAX,zyciePostaci
 mov EBX, zaIle
 sub EAX,EBX
 mov zyciePostaci, EAX
 koniecWykonanieCiosu:
 ret
 wykonanieCiosu endp

 uczieczkaMetoda PROC r :DWORD , piorytet :DWORD
 mov EBX,r
 cmp EBX,3d
 jne koniecum
 mov EBX, piorytet
 cmp EBX, 9d
 je ucieklum
 cmp EBX, 2d
 je udaloUcieklum
 cmp EBX, 1d
 je ledowoUcieklum
 ;powieadomienie nie uciekl
  invoke StdOut ,OFFSET nieucieklTekst
 ret
 ucieklum:
 invoke StdOut ,OFFSET ucieklTekst
 mov blokada,1d
 ret
 udaloUcieklum:
  invoke StdOut ,OFFSET ucieklTekst
  mov blokada,1d
ret
ledowoUcieklum:
 invoke StdOut ,OFFSET ucieklTekst
 mov blokada,1d
 koniecum:
ret
uczieczkaMetoda endp

 metodaCiosuTragicznego PROC ktory :DWORD
	mov EAX, ktory
	cmp EAX, 0d
	je mysleniemct
	cmp EAX, 1d
	je walczeniemct
	cmp EAX,2d
	je szukaniemct
	jmp kocniecmct

	mysleniemct:
	invoke StdOut ,OFFSET ciosTRAGICZNY
	jmp kocniecmct
	walczeniemct:
	invoke StdOut ,OFFSET ciosTRAGICZNYW
	jmp kocniecmct
	szukaniemct:
	invoke StdOut ,OFFSET ciosTRAGICZNYS
	jmp kocniecmct
	kocniecmct:
 RET
 metodaCiosuTragicznego endp

 
 metodacosSieUdalo PROC ktory :DWORD
	mov EAX, ktory
	cmp EAX, 0d
	je mysleniemct
	cmp EAX, 1d
	je walczeniemct
	cmp EAX,2d
	je szukaniemct
	jmp kocniecmct

	mysleniemct:
	invoke StdOut ,OFFSET cioscosSieUdalo
	jmp kocniecmct
	walczeniemct:
	invoke StdOut ,OFFSET cioscosSieUdaloW
	jmp kocniecmct
	szukaniemct:
	invoke StdOut ,OFFSET cioscosSieUdaloS
	jmp kocniecmct
	kocniecmct:
 RET
 metodacosSieUdalo endp

  metodaudaloSie PROC ktory :DWORD
	mov EAX, ktory
	cmp EAX, 0d
	je mysleniemct
	cmp EAX, 1d
	je walczeniemct
	cmp EAX,2d
	je szukaniemct
	jmp kocniecmct

	mysleniemct:
	invoke StdOut ,OFFSET ciosudaloSie
	jmp kocniecmct
	walczeniemct:
	invoke StdOut ,OFFSET ciosudaloSieW
	jmp kocniecmct
	szukaniemct:
	invoke StdOut ,OFFSET ciosudaloSieS
	jmp kocniecmct
	kocniecmct:
 RET
 metodaudaloSie endp

   metodaprawiePerfect PROC ktory :DWORD
	mov EAX, ktory
	cmp EAX, 0d
	je mysleniemct
	cmp EAX, 1d
	je walczeniemct
	cmp EAX,2d
	je szukaniemct
	jmp kocniecmct

	mysleniemct:
	invoke StdOut ,OFFSET ciosprawiePerfect
	jmp kocniecmct
	walczeniemct:
	invoke StdOut ,OFFSET ciosprawiePerfectW
	jmp kocniecmct
	szukaniemct:
	invoke StdOut ,OFFSET ciosprawiePerfectS
	jmp kocniecmct
	kocniecmct:
 RET
 metodaprawiePerfect endp
 

  metodaciosPERFECT PROC ktory :DWORD
	mov EAX, ktory
	cmp EAX, 0d
	je mysleniemct
	cmp EAX, 1d
	je walczeniemct
	cmp EAX,2d
	je szukaniemct
	jmp kocniecmct

	mysleniemct:
	invoke StdOut ,OFFSET ciosPERFECT
	jmp kocniecmct
	walczeniemct:
	invoke StdOut ,OFFSET ciosPERFECTW
	jmp kocniecmct
	szukaniemct:
	invoke StdOut ,OFFSET ciosPERFECTS
	jmp kocniecmct
	kocniecmct:
 RET
 metodaciosPERFECT endp

atoi proc uses esi edx inputBuffAddr:DWORD
	mov esi, inputBuffAddr
	xor edx, edx
	xor EAX, EAX
	mov AL, BYTE PTR [esi]
	cmp eax, 2dh
	je parseNegative

	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0
	mov EAX, EDX
	jmp endatoi

	parseNegative:
	inc esi
	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0

	xor EAX,EAX
	sub EAX, EDX
	jmp endatoi

	endatoi:
	ret
atoi endp



END	